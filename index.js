(function () {
    const bitwiseAND = (num1, num2) => {
        return Number((num1 & num2).toString(2));
    };
    const bitwiseOR = (num1, num2) => {
        return Number((num1 | num2).toString(2));
    };
    const bitwiseXOR = (num1, num2) => {
        return Number((num1 ^ num2).toString(2));
    };
})();
